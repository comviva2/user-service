package com.comviva.kafka.util;

import java.time.format.DateTimeFormatter;

public interface DateConstant {

	String DATE_FORMAT= "yyyy-MM-dd";
	DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DateConstant.DATE_FORMAT);

}
