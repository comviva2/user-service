package com.comviva.kafka.api.response;

public class UserResponse {
	
	private String id;
	
	//No argument constructor for JSON processing
	public UserResponse() {
		
	}

	public UserResponse(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "UserResponse [id=" + id + "]";
	}
	
	
}
