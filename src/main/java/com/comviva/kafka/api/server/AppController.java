package com.comviva.kafka.api.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.comviva.kafka.api.request.UserRequest;
import com.comviva.kafka.command.service.UserService;
import com.comviva.kafka.entity.User;
import com.comviva.kafka.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class AppController {

	@Autowired
	private UserService service;

	@Autowired
	private UserRepository userRepo;

	@GetMapping("/")
	public String viewHomePage() {
		return "index";
	}

	@GetMapping("/register")
	public String showRegistrationForm(@ModelAttribute("userRequest") UserRequest userRequest, Model model) {
		model.addAttribute("user", userRequest);

		return "signup_form";
	}

	@PostMapping(value = "/process_register")
	public String processRegister(@RequestBody @ModelAttribute("userRequest") UserRequest userRequest,Model model) throws JsonProcessingException {
		model.addAttribute("userRequest", userRequest);
		service.register(userRequest);
		return "register_success";
	}

	@GetMapping("/users")
	public String listUsers(Model model) {
		List<User> listUsers = userRepo.findAll();
		model.addAttribute("listUsers", listUsers);

		return "users";
	}

}