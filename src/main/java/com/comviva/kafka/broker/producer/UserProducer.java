package com.comviva.kafka.broker.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.comviva.kafka.broker.message.UserMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserProducer {

	private static final Logger log = LoggerFactory.getLogger(UserProducer.class);

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public static final ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();

	public void publish(UserMessage message) throws JsonProcessingException {
		var json = objectMapper.writeValueAsString(message);
		System.out.println("Raw Json being sent:"+ json);
		kafkaTemplate.send("transaction.data", json)
				.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

					@Override
					public void onSuccess(SendResult<String, String> result) {
						log.info("Send result success for message {}", result.getProducerRecord().value());
					}

					@Override
					public void onFailure(Throwable ex) {
						log.error("Error publishing {}, cause {}", message, ex.getMessage());
					}
				});
	}
}
