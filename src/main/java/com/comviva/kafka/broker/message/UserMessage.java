package com.comviva.kafka.broker.message;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserMessage {

	private String transactionAmount;
	private String currency;
	private String paymentMethod;

	private String senderIdType;
	private String senderIdValue;

	private String receiverIdType;
	private String receiverIdValue;

	public UserMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMessage(String transactionAmount, String currency, String paymentMethod, String senderIdType,
			String senderIdValue, String receiverIdType, String receiverIdValue) {
		super();
		this.transactionAmount = transactionAmount;
		this.currency = currency;
		this.paymentMethod = paymentMethod;
		this.senderIdType = senderIdType;
		this.senderIdValue = senderIdValue;
		this.receiverIdType = receiverIdType;
		this.receiverIdValue = receiverIdValue;
	}

	public String getCurrency() {
		return currency;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public String getReceiverIdType() {
		return receiverIdType;
	}

	public String getReceiverIdValue() {
		return receiverIdValue;
	}

	public String getSenderIdType() {
		return senderIdType;
	}

	public String getSenderIdValue() {
		return senderIdValue;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void setReceiverIdType(String receiverIdType) {
		this.receiverIdType = receiverIdType;
	}

	public void setReceiverIdValue(String receiverIdValue) {
		this.receiverIdValue = receiverIdValue;
	}

	public void setSenderIdType(String senderIdType) {
		this.senderIdType = senderIdType;
	}

	public void setSenderIdValue(String senderIdValue) {
		this.senderIdValue = senderIdValue;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	@Override
	public String toString() {
		return "UserMessage [transactionAmount=" + transactionAmount + ", currency=" + currency + ", paymentMethod="
				+ paymentMethod + ", senderIdType=" + senderIdType + ", senderIdValue=" + senderIdValue
				+ ", receiverIdType=" + receiverIdType + ", receiverIdValue=" + receiverIdValue + "]";
	}

	@JsonProperty("receiver")
	private void unpackNestedReceiver(Map<String, Object> receiver) {
		this.receiverIdType = (String) receiver.get("idType");
		this.receiverIdValue = (String) receiver.get("idValue");
	}

	@JsonProperty("sender")
	private void unpackNestedSender(Map<String, Object> sender) {
		this.senderIdType = (String) sender.get("idType");
		this.senderIdValue = (String) sender.get("idValue");
	}

}
