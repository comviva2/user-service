package com.comviva.kafka.command.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comviva.kafka.api.request.UserRequest;
import com.comviva.kafka.broker.message.UserMessage;
import com.comviva.kafka.broker.producer.UserProducer;
import com.comviva.kafka.entity.User;
import com.comviva.kafka.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
public class UserAction {

	@Autowired
	private UserProducer producer;
	
	@Autowired
	private UserRepository repo;

	public User convertToUser(UserRequest request) {

		var result = new User();
		result.setEmail(request.getEmail());
		result.setFirstName(request.getFirstName());
		result.setLastName(request.getLastName());

		result.setPassword(request.getPassword());

		result.setPhoneNo(request.getPhoneNo());
		result.setDob(request.getDob());

		// finally returning the User Result object
		return result;
	}

	public void save(User user) {

		repo.save(user);
	}

	public void publishToKafka(User user) throws JsonProcessingException {
		var userMessage = new UserMessage();

		userMessage.setSenderIdType("email");
		userMessage.setSenderIdValue(user.getEmail());
		
		producer.publish(userMessage);

	}

}
