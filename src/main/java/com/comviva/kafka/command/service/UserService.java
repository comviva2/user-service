package com.comviva.kafka.command.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.comviva.kafka.api.request.UserRequest;
import com.comviva.kafka.command.action.UserAction;
import com.comviva.kafka.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class UserService {

	@Autowired
	private UserAction action;

	public void register(UserRequest request) throws JsonProcessingException {

		// Convert UserRequest object to database object(class: User)
		User user = action.convertToUser(request);
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);

		// save this user entity to database
		action.save(user);

		// each user detail to be flattened and elements mapped to kafka message
		action.publishToKafka(user);

		//return "register_success";
	}

}
